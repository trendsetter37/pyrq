import setuptools

setuptools.setup(
    name='pyrq',
    version='0.1.0',
    url='https://bitbucket.org/trendsetter37/pyrq',

    author='Javis Sullivan',
    author_email='javis@jaybytes.io',

    description='Many hands make like work',
    entry_points={
        'console_scripts': [
            'pyrq=pyrq.__main__:main'
        ]
    },
    long_description=open('README.rst').read(),

    packages=setuptools.find_packages(),

    install_requires=[],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
    ],
)
