# Sample Test passing with nose and pytest

import os

import pytest

from pyrq import utils
from pyrq.lib import balance
from pyrq.utils import IRQueue
from pyrq.utils import PinnedQueue 

DIRECTORY = os.path.dirname(__file__)
_join = os.path.join
DATA_PATH = os.path.abspath(_join(DIRECTORY, 'data'))
TEST_FILE = _join(DATA_PATH, 'example.txt')


def test_file_parsing():
    stats = utils.stats_from_file(TEST_FILE)
    assert stats['cores'] == ['CPU0', 'CPU1']
    assert len(stats['queues']) == 65
    assert stats['queues'][0] == IRQueue(*['132', 7805535, 2676698559, 2684504094])
    assert stats['total'] == 54954968127
    assert stats['cpu0_affinity_initial'] == pytest.approx(0.591, 1e-2)
    assert stats['cpu1_affinity_initial'] == pytest.approx(0.408, 1e-2)


def test_balance_function():
    irq_info = utils.stats_from_file(TEST_FILE)
    balanced_info = balance(irq_info)
    assert balanced_info['balanced_queues'][0] == PinnedQueue(name='164', interrupts=34688, core='CPU0')
    assert balanced_info['balanced_queues'][-1] ==  PinnedQueue(name='136', interrupts=4750758796, core='CPU0')
    assert balanced_info['cpu0_affinity_final'] == pytest.approx(0.518, 1e-2)
    assert balanced_info['cpu1_affinity_final'] == pytest.approx(0.481, 1e-2)
