irq_balance
===========

.. image:: https://img.shields.io/pypi/v/irq_balance.svg
    :target: https://pypi.python.org/pypi/irq_balance
    :alt: Latest PyPI version

.. image:: https://travis-ci.org/trendsetter37/irq_balance.png
   :target: https://travis-ci.org/trendsetter37/irq_balance
   :alt: Latest Travis CI build status

Many hands make like work

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`irq_balance` was written by `Javis Sullivan <javis@jaybytes.io>`_.
