# -*- coding: utf-8 -*-

from .utils import PinnedQueue


def balance(stats):
    '''Stats received will be of the form
       {
         'cores': ['CPU0', 'CPU1'],
         'cpu0_affinity_initial': 0.591,
         'cpu1_affinity_initial': 0.408,
         'queues': [
           IRQueue(name='123', cpu0=456, cpu1=433, irq_sum=889),
           ...,
           ...,
           IRQueue(name='124', cpu0=45, cpu1=523, irq_sum=568),
         ]
       }

       returns new_stats object
       {
         'cores': ['CPU0', 'CPU1'],
         'cpu0_total': 45876,
         'cpu1_total': 44966,
         'cpu0_affinity_initial': 0.591,
         'cpu1_affinity_initial': 0.408,
         'cpu0_affinity_final': 0.510,
         'cpu1_affinity_final': 0.483,
         'balanced_queues': [
           PinnedQueuee(name='123', interrupts=2000, cpu='CPU0'),
           ...,
           ...,
           PinnedQueuee(name='164', interrupts=1999, cpu='CPU1'),
         ]
       }

    '''
    cpu_frob = -1  # using this because only 2 cpu in contrived example
    sorted_queues = sorted(stats['queues'], key=lambda x: x.irq_sum)
    new_stats = {
        'cpu0_total': 0,
        'cpu1_total': 0,
        'cpu0_affinity_initial': stats['cpu0_affinity_initial'],
        'cpu1_affinity_initial': stats['cpu1_affinity_initial'],
        'cpu0_affinity_final': -1,
        'cpu1_affinity_final': -1,
        'balanced_queues': []
    }
    for queue in sorted_queues:
        name = queue.name
        interrupts = queue.irq_sum
        cpu = 'CPU0' if cpu_frob == -1 else 'CPU1'
        if cpu is 'CPU0':
            new_stats['cpu0_total'] += interrupts
        else:
            new_stats['cpu1_total'] += interrupts
        new_stats['balanced_queues'].append(PinnedQueue(name, interrupts, cpu))
        cpu_frob *= -1
    new_stats['cpu0_affinity_final'] = new_stats['cpu0_total'] / float(stats['total'])
    new_stats['cpu1_affinity_final'] = new_stats['cpu1_total'] / float(stats['total'])

    return new_stats
