#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import pprint

from utils import stats_from_file
from lib import balance


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                        default='/proc/interrupts',
                        help='A file with Tabular queue data typically located at /proc/interrupts')
    return parser.parse_args()

def main():
   args = get_args() 
   stats = stats_from_file(args.file)
   balanced_stats = balance(stats)
   pprint.pprint(stats, indent=4)
   pprint.pprint(balanced_stats, indent=4)

if __name__ == '__main__':
    main()
