# -*- coding: utf-8 -*-

from collections import namedtuple

IRQueue = namedtuple('IRQueue', ['name', 'cpu0', 'cpu1', 'irq_sum'])
PinnedQueue = namedtuple('PinnedQueue', ['name', 'interrupts', 'core'])

def file_to_lines(file):
    with open(file) as f:
        return f.readlines()


def parse_lines(lines):
    '''Returns a stats object from queue data'''
    length = len(lines)
    stats = dict()
    queues = list()
    total = 0
    cpu0_total = 0
    cpu1_total = 0
    for idx, line in enumerate(lines):
        if idx == 0:
            stats['cores'] = [x.strip() for x in line.split(' ')] 
        else:
            queue_label, values = line.split(':')
            core_amount = len(stats['cores'])
            interrupt_vals = [int(x) for x in values.strip().split(' ', core_amount)[0:core_amount]]
            queue_sum = sum(interrupt_vals)
            total += queue_sum
            cpu0_total += interrupt_vals[0]
            cpu1_total += interrupt_vals[1]
            queues.append(IRQueue(*([queue_label] + interrupt_vals + [queue_sum])))
    stats['queues'] = queues
    stats['total'] = total
    stats['cpu0_affinity_initial'] = cpu0_total / float(total) 
    stats['cpu1_affinity_initial'] = cpu1_total / float(total)
    return stats


def stats_from_file(file):
    return parse_lines(file_to_lines(file))
